package network;

import java.io.IOException;
import java.io.OptionalDataException;
import java.io.UncheckedIOException;
import java.net.*;
import java.util.Arrays;

public class Connection {

    private MulticastSocket socket;
    private InetAddress groupIp;
    //    private static byte[] msgAsk = {1};
//    private static byte[] msgAnswer = {2};
    private String msgAsk = "Hi, I'm here baby";
    private String msgAnswer = "Hi, I'm here, too";

    public Connection(String ip) throws IOException {
        groupIp = InetAddress.getByName(ip);
        socket = new MulticastSocket(6790);
        socket.joinGroup(groupIp);
        socket.setSoTimeout(3000);
    }

    public void listen() throws IOException, TypeNotPresentException {
        byte[] buf = new byte[msgAnswer.getBytes().length]; // буфер для получения датаграммы размера ???
        DatagramPacket hi = new DatagramPacket(msgAsk.getBytes(), msgAsk.getBytes().length, groupIp, 6790); //создание датаграммы аска
        socket.send(hi);
        IpPresenter presenter = new IpPresenter();
        DatagramPacket rcvPacket = new DatagramPacket(buf, buf.length);  // создание датаграммы приема
        while (true) {
            try {
                socket.receive(rcvPacket);
                if (Arrays.equals(rcvPacket.getData(), msgAsk.getBytes())) {
                    presenter.addAddress(new IpAddress(rcvPacket.getAddress().getHostAddress()));
                    hi = new DatagramPacket(msgAnswer.getBytes(), msgAnswer.getBytes().length, rcvPacket.getAddress(), 6790);
                    socket.send(hi);
                } else if (Arrays.equals(rcvPacket.getData(), msgAnswer.getBytes())) {
                    presenter.addAddress(new IpAddress(rcvPacket.getAddress().getHostAddress()));
                } else {
                    //throw new TypeNotPresentException("Data loss", null);
                    continue;
                }
            } catch (SocketTimeoutException e) {
                hi = new DatagramPacket(msgAsk.getBytes(), msgAnswer.getBytes().length, groupIp, 6790);    //создание датаграммы аска
                socket.send(hi);
                presenter.checking();
            }
            presenter.checking();
        }
    }
}