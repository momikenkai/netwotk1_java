package network;

import java.util.ArrayList;
import java.util.List;

public class IpPresenter {
    private List<IpAddress> addresses;

    public IpPresenter() {
        addresses = new ArrayList<>();
    }

    public void addAddress(IpAddress ip) {
        if (addresses.contains(ip)) {
            addresses.get(addresses.indexOf(ip)).updateTime();
        } else {
            addresses.add(ip);
            System.out.println(ip);
        }
    }

    private void redraw() {
        System.out.println("---------------");
        addresses.forEach(System.out::println);
    }

    public void checking() {
        long currentTime = System.currentTimeMillis();
        boolean change = false;
        for (int i = 0; i < addresses.size(); ++i) {
            if (currentTime - addresses.get(i).getTime() > 6 * 1000) {
                addresses.remove(addresses.get(i));
                change = true;
            }
        }
        if (change) redraw();
    }
}
